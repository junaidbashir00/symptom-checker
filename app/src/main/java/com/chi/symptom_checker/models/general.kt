package com.chi.symptom_checker.models

import android.graphics.Color
import com.chi.symptom_checker.R
import com.codegini.lib.controls.forms.models.FieldType
import com.google.gson.Gson
import kotlin.collections.ArrayList


data class SymptomsObservation(
    val reading_id: Int?,
    val reading_time: Long?,
    var acquisition_time: Long?,
    val service: String?,
    val service_to_display: String?,
    val status: String?,
    val status_to_display: String?,
    val text_value: String,
    val date_added: Long?
)
{
    val statusColor: Int get()
    {
        var c = R.color.colorPrimary
        when(status)
        {

            "Warning", "Warning Low", "Warning High", "Ultra Warning Low", "Ultra Warning High" -> c = Color.YELLOW
            "Critical", "Critical Low", "Critical High", "Ultra Critical Low", "Ultra Critical High" -> c =  Color.RED
        }

        return  c
    }
}

data class SymptomsObservationList(
    val data: List<SymptomsObservation>,
    val total_records: Int
)

data class SymptomsDetails(
    val complaint: String,
    val age: Int,
    val sex: String,
    val results: List<Condition>,
    val evidence: List<EvidenceDetail>
)

data class EvidenceDetail(
    var id: String? = null,
    var name: String? = null,
    var choice_id: String? = null,
    var question: String? = null,
    var question_type: String? = null,
    var initial: Boolean? = null,
    var related: Boolean? = null
)

data class ExplainSymptom(
    var id: String? = null,
    var name: String? = null,
    var common_name: String? = null
)

data class ExplainResponse(
    val supporting_evidence: List<ExplainSymptom>,
    val conflicting_evidence: List<ExplainSymptom>,
    val unconfirmed_evidence: List<ExplainSymptom>
)

data class SymptomateMentions(
    var mentions: ArrayList<Mentions>? = null,
    val obvious: Boolean? = null
)

data class Mentions(
    var id: String? = null,
    val name: String? = null,
    val common_name: String? = null,
    val orth: String? = null,
    val choice_id: String? = null,
    val type: String? = null
)

data class SymptomateRiskFactors(
    var data: ArrayList<RiskFactor>? = null
)

data class RiskFactor(
    var id: String? = null,
    val name: String? = null,
    val common_name: String? = null,
    val category: String? = null,
    val seriousness: String? = null,
    val sex_filter: String? = null
)

data class Suggestion(
    var sex: String? = null,
    var age: String? = null,
    var selected: ArrayList<String>? = null
)

data class SuggestionResponse(
    var id: String? = null,
    var name: String? = null,
    var common_name: String? = null
)

data class SymptomateQuestion(
    var question: Question? = null,
    var conditions: ArrayList<Condition>? = null,
    var should_stop: Boolean? = null
)

data class Question(
    var type: String? = null,
    val text: String? = null,
    var items: ArrayList<Items>? = null
)

data class Condition(

    var id: String? = null,
    val name: String? = null,
    var common_name: String? = null,
    val probability: Float? = null,
    var supporting_evidence: ArrayList<String>? = null,
    var conflicting_evidence: ArrayList<String>? = null,
    var unconfirmed_evidence: ArrayList<String>? = null
)
{
    val Percentage: String
        get() {
            return (probability!! * 100).toInt().toString() + "%"
        }

    val Progress: Int
        get() {
            return (probability!! * 100).toInt()
        }
}

data class Items(
    var id: String? = null,
    var name: String? = null,
    var choices: Array<Choice>? = null
)

data class Choice(
    var id: String? = null,
    var label: String? = null
)

data class LastConvertedRequest(
    var complaint: String? = null,
    var target: String? = null,
    var sex: String? = null,
    var age: String? = null,
    var evidence: ArrayList<SymptomateAnswer>? = null,
    var conditions: ArrayList<Condition>? = null
)
{
    val deepCopy: LastConvertedRequest
        get() {
            val JSON = Gson().toJson(this)
            return Gson().fromJson(JSON, LastConvertedRequest::class.java)
        }
}

data class SymptomateAnswer(
    var id: String? = null,
    var choice_id: String? = null,
    var initial: Boolean? = null,
    var related: Boolean? = null,
    var question: String? = null,
    var answer: String? = null,
    var question_type: String? = null
)

data class EventQuestion(
    var event_question_id: Int? = null,
    var question_id: String? = null,
    var question_category_id: Int? = null,
    val title: String? = null,
    val title_to_display: String? = null,
    val is_required: Boolean = false,
    val comments_required: Boolean? = null,
    var on_set_time: Boolean? = null,
    var type: FieldType? = null,
    var options: ArrayList<Option>? = null,
    var units: Array<Unit>? = null
)

data class Option(
    var option_id: String? = null,
    var option_name: String? = null,
    var option_name_to_display: String? = null
)

data class Unit(
    var unit_id: Int? = null,
    var unit_name: String? = null
)

data class EventAnswer(
    var event_question_id: Int? = null,
    var question: String? = null,
    var answer: String? = null,
    var on_set_time: Long? = null,
    var units: String? = null,
    var comments: String? = null,
    var option_id: String? = null,
    var unselected_id: String? = null,
    var type: FieldType? = null
)

class EventAnswerData (
    var reading_id: Int? = null,
    var date_added: Long? = null,
    var answers: ArrayList<EventAnswer> = ArrayList()
)

data class SymptomateObservation (
    var last_converted_request: LastConvertedRequest? = null,
    var conditions: ArrayList<Condition>? = null
)

data class QuestionsList (
    var list: ArrayList<QuestionsData>? = null
)

data class QuestionsData(
    var question: String? = null,
    var answer: String? = null,
    var option_id: String? = null,
    var question_type: String? = null
)

data class AnswerData(
    var selected_ids: String? = null,
    var choice_id: String? = null,
    var answer_text: String? = null,
    var unseleted_ids: String? = null,
    var question_type: String? = null
)