package com.chi.symptom_checker

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.chi.symptom_checker.common.GenericAdapter
import com.chi.symptom_checker.models.*
import com.chi.symptom_checker.utils.Preferences
import com.chi.symptom_checker.databinding.SymptomDetailActivityBinding
import com.codegini.lib.controls.datatable.DataTableConfig
import com.codegini.lib.controls.datatable.DataTableFragment
import com.codegini.lib.controls.datatable.DataTableListener
import com.codegini.lib.controls.datatable.DataTableProvider
import com.codegini.lib.controls.forms.models.FieldType
import kotlin.collections.ArrayList

class SymptomDetailActivity : AppBaseActivity(), DataTableProvider, DataTableListener<Condition>
{
    private val binding: SymptomDetailActivityBinding by ActivityBindingProperty(R.layout.symptom_detail_activity)
    var resultList: ArrayList<Condition>? = null
    var mSuggestionList: ArrayList<Mentions>? = ArrayList()
    var suggestionsAdapter: GenericAdapter<Mentions>? = null
    var mQuestionsList: QuestionsList? = QuestionsList(ArrayList())
    var questionsAdapter: GenericAdapter<QuestionsData>? = null
    var mReadingId: Int = 0
    private val rotate = false
    var explainRequest: LastConvertedRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        binding.appBar.lblTitle.text = "Symptom Detail"

        resultList = ArrayList()

        val intent = intent
        mReadingId = intent.getIntExtra("reading_id",0)
//        mQuestionsList = deserialize(intent.getStringExtra("questions")!!, QuestionsList::class.java)

        initDataTable()
    }

    private fun initDataTable() {
        val fragment =
            DataTableFragment.newInstance<Condition>("sc_detail")
        val fragmentManager =
            this.supportFragmentManager
        val fragmentTransaction =
            fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.lorContent, fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun onAppbarBack(v: View) {
        finish()
    }

    override fun onBackPressed() {
        finish()
    }

    fun showSuggestions(resultList: List<EvidenceDetail>)
    {
        mSuggestionList!!.clear()
        for (result in resultList)
        {
            if (result.initial!!)
            {
                mSuggestionList!!.add(Mentions(result.id, result.name, result.name, null, result.choice_id, null))
            }
        }

        suggestionsAdapter = GenericAdapter(mSuggestionList!!, this, R.layout.cell_suggestion_list_no_trash)
        { view: View, it: Mentions ->

        }
        binding.rvSuggestion.adapter = suggestionsAdapter
    }

    fun createExplainRequest(data: SymptomsDetails)
    {
        explainRequest = LastConvertedRequest(null,null, data.sex, data.age.toString(), ArrayList(), null)
        for (evidence in data.evidence)
        {
            explainRequest!!.evidence!!.add(SymptomateAnswer(evidence.id, evidence.choice_id, evidence.initial, evidence.related))
        }
    }

    fun createQuestionList(resultList: List<EvidenceDetail>)
    {
        mQuestionsList!!.list!!.clear()
        for (result in resultList)
        {
            if (result.question != null)
            {
                if (result.question_type == FieldType.DF_SINGLE.toString() || result.question_type == FieldType.DF_MULTI_CHOICE.toString())
                {
                    var choice = "Don't know"
                    when {
                        result.choice_id.equals("present") -> choice = "Yes"
                        result.choice_id.equals("absent") -> choice = "No"
                    }

                    mQuestionsList!!.list!!.add(QuestionsData(result.question, choice, result.id))
                }
                else
                    mQuestionsList!!.list!!.add(QuestionsData(result.question, result.name, result.id))
            }
        }

        questionsAdapter = GenericAdapter(mQuestionsList!!.list!!, this, R.layout.cell_question_list)
        { view: View, it: QuestionsData ->

        }
        binding.rvQuestions.adapter = questionsAdapter
    }

    override fun getDataTableConfig(p0: DataTableFragment<*>?): DataTableConfig {
        val config = DataTableConfig()
        config.noRecordCell = R.layout.datatable_norecord
        config.cellResourceId = R.layout.summary_table_cell
//        config.animation_type = ItemAnimation.FADE_IN;

        //        config.animation_type = ItemAnimation.FADE_IN;
        config.showSearch = false
        config.showAddButton = false

        return config
    }

    override fun getDataTableListener(p0: DataTableFragment<*>?): DataTableListener<*> {
        return this
    }

    override fun loadData(fragment: DataTableFragment<Condition>?, p1: Int, p2: String?) {
        val req = SymptomsDetailRequest(mReadingId)

//        val call = apiService.getSymptomsDetail(req)
//        call.enqueue(object : ApiCallback<ApiSingleResponse<SymptomsDetails>>(this@SymptomDetailActivity)
//        {
//            override fun onSuccess(resp: ApiSingleResponse<SymptomsDetails>)
//            {
//                fragment!!.onAppendData(resp.data.results, resp.data.results.size)
//                showSuggestions(resp.data.evidence)
//                binding.lblTitle.text = resp.data.complaint
//                binding.lblTitle.visibility = View.VISIBLE
//                createExplainRequest(resp.data)
//                createQuestionList(resp.data.evidence)
//            }
//
//            override fun onError(t: CgThrowable)
//            {
//                showAlert(getString("error_loading_data", R.string.error_loading_data), t.ErrorMessage)
//            }
//        })
    }

    override fun onCellClick(p0: View?)
    {
        val item = p0!!.tag as Condition
        explainRequest!!.target = item.id
        val intent = Intent(this, SymptomExplainActivity::class.java)
        intent.putExtra("explainRequest", Preferences.serialize(explainRequest))
        startActivity(intent)
    }

    override fun onCellLongClick(p0: View?, p1: Condition?): Boolean {
        return false
    }

    override fun onAddClick(p0: DataTableFragment<Condition>?) {}
}
