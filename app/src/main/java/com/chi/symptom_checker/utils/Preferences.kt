package com.chi.symptom_checker.utils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.provider.Settings
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import androidx.annotation.StringRes
import com.google.gson.Gson
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.security.*
import java.text.SimpleDateFormat
import java.util.*
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.security.auth.x500.X500Principal

object Preferences
{
    private val KEY_AUTH_TOKEN = "auth-token"
    private val KEY_DEVICE_ID = "device-id"
    private val KEY_SUBJECT_ID = "subject-id"
    private val KEY_PATIENT = "patient"
    val GCM_REGISTRATION_COMPLETE = "GCM_REGISTRATION_COMPLETE"


    private val ALIAS = "chi.patient.app-prefs"
    private val KEYSTORE = "AndroidKeyStore"
    private val TYPE_RSA = "RSA"
    private val CYPHER = "RSA/ECB/PKCS1Padding"
    private val ENCODING = "UTF-8"
    private val GENERAL_PREFS = "chi.patient.general-prefs"

    fun getAuthToken(context: Context): String?
    {
        return get(context, KEY_AUTH_TOKEN)
//        return PreferenceManager.getDefaultSharedPreferences(context)
//            .getString(KEY_AUTH_TOKEN, null)
    }


    fun saveAuthToken(context: Context, token: String)
    {
        put(context, KEY_AUTH_TOKEN, token)
//        val editor = PreferenceManager.getDefaultSharedPreferences(context)
//            .edit()
//
//        editor.putString(KEY_AUTH_TOKEN, token)
//        editor.apply()
    }

    fun clearAuthToken(context: Context)
    {
        put(context, KEY_AUTH_TOKEN, null)
//        PreferenceManager.getDefaultSharedPreferences(context)
//            .edit()
//            .remove(KEY_AUTH_TOKEN)
//            .apply()
    }

    fun getAppVersion(context: Context): String
    {
        try
        {
            return context.packageManager.getPackageInfo(context.packageName, 0).versionName
        }
        catch (e: Exception)
        {
            return "0.0.0"
        }
    }

    fun removeKey(context: Context, @StringRes keyId: Int)
    {
        Preferences.removeKey(context, context.getString(keyId))
    }

    fun removeKey(context: Context, key: String)
    {
        put(context, key, null)
//        PreferenceManager.getDefaultSharedPreferences(context)
//            .edit()
//            .remove(key)
//            .apply()
    }


    fun getDeviceId(context: Context): String?
    {
        val prefs = context.getSharedPreferences(ALIAS, MODE_PRIVATE)

        var deviceId = prefs.getString(KEY_DEVICE_ID, null)

        if (deviceId == null)
        {
            deviceId = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)

//            PreferenceManager.getDefaultSharedPreferences(context)
//                .edit()
//                .putString(KEY_DEVICE_ID, deviceId)
//                .apply()
            prefs.edit().putString(KEY_DEVICE_ID,deviceId).apply()
        }

        return deviceId
    }

    fun setString(context: Context, @StringRes resId: Int, value: String)
    {
        put(context,context.getString(resId),value)
//        PreferenceManager.getDefaultSharedPreferences(context)
//            .edit()
//            .putString(context.getString(resId), value)
//            .apply()
    }

    fun setString(context: Context, resId: String, value: String)
    {
        put(context,resId,value)
//        PreferenceManager.getDefaultSharedPreferences(context)
//            .edit()
//            .putString(resId, value)
//            .apply()
    }

    fun getString(context: Context, @StringRes resId: Int): String?
    {
        return get(context,context.getString(resId))
//        return PreferenceManager.getDefaultSharedPreferences(context)
//            .getString(context.getString(resId), null)
    }

    fun getString(context: Context, resId: String): String?
    {
        return get(context,resId)
//        return PreferenceManager.getDefaultSharedPreferences(context)
//            .getString(resId, null)
    }

    fun getLong(context: Context, id: String): Long
    {
        val v = get(context, id)
        if (v == null)
            return -1

        return v.toLong()

//        return PreferenceManager.getDefaultSharedPreferences(context)
//            .getLong(id, -1)
    }

    fun setLong(context: Context, id: String, value: Long)
    {
        put(context,id,value.toString())
//        PreferenceManager.getDefaultSharedPreferences(context)
//            .edit()
//            .putLong(id, value)
//            .apply()
    }

    fun getInt(context: Context, id: String): Int
    {
        val v = get(context, id)
        if (v == null)
            return 0

        return v.toInt()

//        return PreferenceManager.getDefaultSharedPreferences(context)
//            .getInt(id, 0)
    }

    fun setInt(context: Context, id: String, value: Int)
    {
        put(context,id,value.toString())
//        PreferenceManager.getDefaultSharedPreferences(context)
//            .edit()
//            .putInt(id, value)
//            .apply()
    }

    fun setBoolean(context: Context, @StringRes resId: Int, value: Boolean)
    {
        put(context,context.getString(resId),value.toString())
//        PreferenceManager.getDefaultSharedPreferences(context)
//            .edit()
//            .putBoolean(context.getString(resId), value)
//            .apply()
    }

    fun setBoolean(context: Context, id: String, value: Boolean)
    {
        put(context,id,value.toString())
//        PreferenceManager.getDefaultSharedPreferences(context)
//            .edit()
//            .putBoolean(id, value)
//            .apply()
    }

    fun getBoolean(context: Context, @StringRes resId: Int): Boolean
    {
        return Preferences.getBoolean(context, resId, false)
    }

    fun getBoolean(context: Context, @StringRes resId: Int, defValue: Boolean): Boolean
    {
        return Preferences.getBoolean(context, context.getString(resId), defValue)
    }

    fun getBoolean(context: Context, id: String, defValue: Boolean = false): Boolean
    {
        val v = get(context, id)
        if (v == null)
            return defValue

        return v.toBoolean()

//        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
//        return prefs.getBoolean(id, defValue)
    }

    fun serialize(obj: Any?): String?
    {
        if (obj == null)
            return null

        val gson = Gson()
        return gson.toJson(obj)
    }

    fun serializeToBytes(obj: Any?): ByteArray?
    {
        if (obj == null)
            return null

        val gson = Gson()
        return gson.toJson(obj).toByteArray()
    }

    fun <T> deserialize(str: String, clazz: Class<T>): T {
        val gson = Gson()
        val obj = gson.fromJson(str, clazz)

        return obj as T
    }

    fun timeStampToCustom(date: Long, format: String): String
    {
        val timestamp = java.lang.Long.parseLong(date.toString()) * 1000L
        val date = Date(timestamp)
        val formatter = SimpleDateFormat(format)
        val stringDate = formatter.format(date)
        return stringDate
    }

    fun timeStampToStringDateTime(date: Long): String
    {
        val timestamp = java.lang.Long.parseLong(date.toString()) * 1000L
        val date = Date(timestamp)
        val formatter = SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a")
        val stringDate = formatter.format(date)
        return stringDate
    }

    fun timeStampToStringDate(date: Long): String
    {
        val timestamp = java.lang.Long.parseLong(date.toString()) * 1000L
        val date = Date(timestamp)
        val formatter = SimpleDateFormat("dd-MMM-yyyy")
        val stringDate = formatter.format(date)
        return stringDate
    }

    fun timeStampToDate(date: Long): Date
    {
        val dt = Date(date)
        return dt
    }

    fun longToDate(date: Long): Date
    {
        val timestamp = java.lang.Long.parseLong(date.toString()) * 1000L
        val date = Date(timestamp)
        return date
    }

    fun currentTimestamp(): Long
    {
        return System.currentTimeMillis() / 1000
    }

    fun saveSubjectId(context: Context, subject_id: Int)
    {
        val editor = PreferenceManager.getDefaultSharedPreferences(context)
            .edit()

        editor.putInt(KEY_SUBJECT_ID, subject_id)
        editor.apply()
    }

    fun getSubjectId(context: Context): Int?
    {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getInt(KEY_SUBJECT_ID, 0)
    }

    fun startOfDay(): Long
    {
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, 0) //set hours to zero
        cal.set(Calendar.MINUTE, 0) // set minutes to zero
        cal.set(Calendar.SECOND, 0) //set seconds to zero
        val time = cal.timeInMillis / 1000
        return time
    }

    fun startOfDay(time: Long): Long
    {
        val cal = Calendar.getInstance()
        cal.time = Date(time * 1000)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        val time = cal.timeInMillis / 1000
        return time
    }

    fun endOfDay(time: Long): Long
    {
        val cal = Calendar.getInstance()
        cal.time = Date(time * 1000)
        cal.set(Calendar.HOUR_OF_DAY, 23)
        cal.set(Calendar.MINUTE, 59)
        cal.set(Calendar.SECOND, 59)
        val time = cal.timeInMillis / 1000
        return time
    }

    fun startOfWeek(time: Long): Long
    {
        val cal = Calendar.getInstance()
        cal.time = Date(time * 1000)
        cal.set(Calendar.DAY_OF_WEEK, cal.firstDayOfWeek)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        val time = cal.timeInMillis / 1000
        return time
    }

    fun endOfWeek(time: Long): Long
    {
        val cal = Calendar.getInstance()
        cal.time = Date(time * 1000)
        cal.set(Calendar.DAY_OF_WEEK, cal.firstDayOfWeek+6)
        cal.set(Calendar.HOUR_OF_DAY, 23)
        cal.set(Calendar.MINUTE, 59)
        cal.set(Calendar.SECOND, 59)
        val time = cal.timeInMillis / 1000
        return time
    }

    fun saveScheduleDaysLimit(ctx: Context, days: Int, key: String)
    {
        put(ctx,key,days.toString())
//        val sp = PreferenceManager.getDefaultSharedPreferences(ctx)
//
//        val editor = sp.edit()
//        editor.putInt(key, days)
//
//        editor.apply()
    }

    fun getScheduleDaysLimit(ctx: Context, key: String): Int
    {
        var days = get(ctx, key)
        if (days == null)
            days ="1"

        return if (days.toInt() == 1) 1 else days.toInt()

//        val sp = PreferenceManager.getDefaultSharedPreferences(ctx)
//
//        val days = sp.getInt(key, 1)
//        return if (days == 1) 1 else days
    }





    operator fun get(ctx: Context, key: String): String?
    {
        val prefs = ctx.getSharedPreferences(ALIAS, MODE_PRIVATE)

//        val key2 = makeString(ctx, key)
        val pref = prefs.getString(key, null) ?: return null

        return breakString(ctx, pref)
    }

    // Keystore Preferences Methods
    fun put(ctx: Context, key: String, value: String?)
    {
        val prefs = ctx.getSharedPreferences(ALIAS, MODE_PRIVATE)
//        val key2 = makeString(ctx, key)

        if (value == null)
        {
            prefs.edit().remove(key).apply()
            return
        }

        prefs.edit().putString(key, makeString(ctx, value)).apply()
    }

    private fun makeString(context: Context, toEncrypt: String): String?
    {
        try
        {
//            val privateKeyEntry = getPrivateKey(context)
            val privateKey = getPrivateKey(context)
            if (privateKey != null)
            {
                val keyStore = KeyStore.getInstance("AndroidKeyStore")
                keyStore.load(null)
                val publicKey = keyStore.getCertificate(ALIAS).publicKey;

                // Encrypt the text
                val input = Cipher.getInstance(CYPHER)
                input.init(Cipher.ENCRYPT_MODE, publicKey)

                val outputStream = ByteArrayOutputStream()
                val cipherOutputStream = CipherOutputStream(
                    outputStream, input
                )
                cipherOutputStream.write(toEncrypt.toByteArray(charset(ENCODING)))
                cipherOutputStream.close()

                val vals = outputStream.toByteArray()
                return Base64.encodeToString(vals, Base64.DEFAULT)
            }
        } catch (e: Exception) {
            return null
        }

        return null
    }

    private fun breakString(context: Context, encrypted: String): String?
    {
        try
        {
            val privateKey = getPrivateKey(context)
            if (privateKey != null)
            {
//                val privateKey = privateKeyEntry.privateKey

                val output = Cipher.getInstance(CYPHER)
                output.init(Cipher.DECRYPT_MODE, privateKey)

                val cipherInputStream = CipherInputStream(
                    ByteArrayInputStream(Base64.decode(encrypted, Base64.DEFAULT)), output
                )

                val values = ArrayList<Byte>()
                var nextByte: Int
                nextByte = cipherInputStream.read()

                while (nextByte != -1)
                {
                    values.add(nextByte.toByte())
                    nextByte = cipherInputStream.read()
                }

                val bytes = ByteArray(values.size)
                for (i in bytes.indices)
                {
                    bytes[i] = values[i]
                }

                return String(bytes, 0, bytes.size, charset(ENCODING))
            }

        }
        catch (e: Exception)
        {
            return null
        }

        return null
    }

//    private fun getPrivateKey(context: Context): KeyStore.PrivateKeyEntry?
//    {
//        var ks = KeyStore.getInstance(KEYSTORE)
//        ks.load(null)
//
//        var entry: KeyStore.Entry? = ks.getEntry(ALIAS, null)
//
//        if (entry == null)
//        {
//            try
//            {
//                createKeys(context)
//            }
//            catch (e: NoSuchProviderException)
//            {
//                e.printStackTrace()
//                return null
//            } catch (e: InvalidAlgorithmParameterException)
//            {
//                e.printStackTrace()
//                return null
//            }
//
//            ks = KeyStore.getInstance(KEYSTORE)
//            ks.load(null)
//
//            entry = ks.getEntry(ALIAS, null)
//
//            if (entry == null)
//            {
//                return null
//            }
//        }
//
//        if (entry !is KeyStore.PrivateKeyEntry)
//        {
//            return null
//        }
//
//        return entry
//    }

    private fun getPrivateKey(context: Context) : PrivateKey?
    {
        var keyStore = KeyStore.getInstance(KEYSTORE)
        keyStore.load(null)

        var privateKey : PrivateKey? = keyStore.getKey(ALIAS, null) as PrivateKey?

        if (privateKey == null)
        {
            try
            {
                createKeys(context)
            }
            catch (e: NoSuchProviderException)
            {
                e.printStackTrace()
                return null
            } catch (e: InvalidAlgorithmParameterException)
            {
                e.printStackTrace()
                return null
            }

            keyStore = KeyStore.getInstance(KEYSTORE)
            keyStore.load(null)

            privateKey = keyStore.getKey(ALIAS, null) as PrivateKey?

            if (privateKey == null)
            {
                return null
            }
        }
        return privateKey
    }

    private fun createKeys(context: Context)
    {
        val start = GregorianCalendar()
        val end = GregorianCalendar()
        end.add(Calendar.YEAR, 25)

        val kpg: KeyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, KEYSTORE);
        val spec = KeyGenParameterSpec.Builder(ALIAS,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
            .setUserAuthenticationRequired(false)
            .setDigests(KeyProperties.DIGEST_SHA256)
            .setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PKCS1)
            .setCertificateSubject(X500Principal("CN=$ALIAS"))
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
//            .setCertificateSerialNumber(BigInteger.valueOf(1337))
            .setCertificateNotBefore(start.time)
            .setCertificateNotAfter(end.time)
            .build()

        kpg.initialize(spec)
        kpg.generateKeyPair()
    }


    fun saveGeneralData(context: Context, key: String, value: String) {
        val sharedPref: SharedPreferences = context.getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getGeneralData(context: Context, key: String): String? {
        val sharedPref: SharedPreferences = context.getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE)
        return sharedPref.getString(key, null)

    }
}