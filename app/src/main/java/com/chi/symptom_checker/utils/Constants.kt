package com.chi.symptom_checker.utils


object Constants
{
    const val MSG_REGISTER_CLIENT = 1
    const val MSG_UNREGISTER_CLIENT = 2
    const val MSG_SET_VALUE = 3

    const val K_MESSAGE_CALLS = 8033

    val RC_ACCESS_FINE_LOCATION = 9001
    val RC_ACCESS_EXTERNAL_STORAGE = 9002
    val RC_ACCESS_CAMERA = 9003
    val RC_ACCESS_MICROPHONE = 9004
    val RC_ALL_PERMISSIONS = 9005

    val ACTION_QUESTION_COUNT = "QuestionCount"
    val ACTION_MESSAGE_COUNT = "MessageCount"
    val ACTION_NOTIFICATION_COUNT = "NotificationCount"

    const val DYNAMIC_READING_RESULT_UID = "DeviceResult"

    val MESSAGE_TEXT = "Message"
    val MEDICATION_TEXT = "Medication"
    val VITALS_TEXT = "Vital"
    val BLOOD_PRESSURE_TEXT = "Blood Pressure"
    val PULSE_TEXT = "Pulse"

    val OXIMeter = "Oximeter"
//    val Ear_Thermometer = "Ear Thermometer"
    val FastingGlucoMeter = "Fasting Blood Glucose"
    val RandomGlucoMeter = "Random Blood Glucose"
    val Weight_Scale = "Weight"
    val SpiroMeter = "Spirometer"
    val Hms6500 = "HMS6500"
    val EKG_TEXT = "ECG"
    val URINE_ANALYZER = "Urine Analyzer"
    val ACTION_CALL_ENDED = "com.on2sol.patientapp.action.CALL_ENDED"
    val NEW_HOMECARE = "com.on2sol.patientapp.action.NEW_HOMECARE"

    val CONTINUOUS_OBSERVATION_STARTED = "Started"
    val CONTINUOUS_OBSERVATION_ONGOING = "Ongoing"
    val CONTINUOUS_OBSERVATION_CLOSED = "Closed"

    val START_PULSE_OX_SVC_START_ACTION = "Start"
    val START_PULSE_OX_SVC_STOP_ACTION = "Stop"

    val START_SEAMATY_SVC_START_ACTION = "Start"
    val START_SEAMATY_SVC_STOP_ACTION = "Stop"

    val START_FETAL_SVC_START_ACTION = "Start"
    val START_FETAL_SVC_STOP_ACTION = "Stop"

    val START_OX_SVC_START_ACTION = "Start"
    val START_OX_SVC_STOP_ACTION = "Stop"
    val current_patient = "Current_Patient"

    // Used by monitoring system - Atiq
    val APP_UI_IS_ALIVE = "app.ui.is.alive"
    val APP_SVC_IS_ALIVE = "app.svc.is.alive"

    val PHONE_STATE = "phone.is.alive"

    val APP_VERSION_ = "app.version"
//    val APP_START_TIME = "app.time.start"
//    val APP_DESTROY_TIME = "app.time.destroyed"

    val BT_ENABLED_STATE = "bt.enabled.state"
    val BT_START_TIME = "bt.time.start"
    val BT_DESTROY_TIME = "bt.time.destroyed"
    val BT_LAST_ACTIVE_TIME = "bt.time.last-active"

//    val PMS_START_TIME = "pms.time.start"
//    val PMS_DESTROY_TIME = "pms.time.destroyed"
    val PMS_ENABLED_STATE = "pms.enabled.state"
    val PMS_LAST_ACTIVE_TIME = "pms.time.last-active"

    val LOC_ENABLED_STATE = "loc.enabled.state"
    val LOC_START_TIME = "loc.time.start"
    val LOC_STOP_TIME = "loc.time.stop"
    val LOC_LAST_ACTIVE_TIME = "loc.time.last-active"

    val INET_ENABLED_STATE = "inet.enabled.state"
    val INET_START_TIME = "inet.time.start"
    val INET_STOP_TIME = "inet.time.stop"
    val INET_LAST_ACTIVE_TIME = "inet.time.last-active"

    val CALL_TYPE_VIDEO = "Video"
    val CALL_TYPE_AUDIO = "Audio"

    const val STATUS_DOCTOR = "Doctors"
    const val STATUS_RECENTS = "Recents"

    const val RECENT_CALL = "CALL"
    const val RECENT_RECORDING = "RECORDING"

    val KEY_APP_LANG = "app-lang"
    val KEY_APP_THEME = "app-theme"
}
