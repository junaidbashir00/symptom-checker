package com.chi.symptom_checker.alert

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import com.chi.symptom_checker.R
import com.chi.symptom_checker.databinding.AlertDialogBinding


class ChiAlerts private constructor(context: Context, private val mAlertType: AlertType)
{
    private val mBinding: AlertDialogBinding
    private var mOnCloseListener: AlertActionListener? = null
    lateinit var dialog: Dialog

    init
    {
        val inflater = LayoutInflater.from(context)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.alert_dialog, null, false)

        when (mAlertType)
        {
            ChiAlerts.AlertType.SuccessAlert -> {
                mBinding.btnClose.setBackgroundColor(context.resources.getColor(R.color.green_700))
            }

            ChiAlerts.AlertType.InfoAlert -> {
                mBinding.btnClose.setBackgroundColor(context.resources.getColor(R.color.colorPrimary))
            }

            ChiAlerts.AlertType.WarningAlert -> {
                mBinding.btnClose.setBackgroundColor(context.resources.getColor(R.color.yellow_700))
            }

            ChiAlerts.AlertType.ErrorAlert -> {
                mBinding.btnClose.setBackgroundColor(context.resources.getColor(R.color.red_800))
            }

            ChiAlerts.AlertType.ConfirmAlert -> {
                mBinding.clButtons.visibility = View.VISIBLE
                mBinding.btnClose.visibility = View.GONE
            }

            ChiAlerts.AlertType.PromptAlert -> {
                mBinding.clButtons.visibility = View.VISIBLE
                mBinding.btnClose.visibility = View.GONE
            }
        }

        if (mAlertType == AlertType.ConfirmAlert) {
            mBinding.btnNo.setOnClickListener { v: View  ->
                dialog.dismiss()

                if (mOnCloseListener != null)
                    mOnCloseListener!!.onAlertAction(false, null)
            }

            mBinding.btnYes.setOnClickListener { v: View  ->
                dialog.dismiss()

                if (mOnCloseListener != null)
                    mOnCloseListener!!.onAlertAction(true, null)
            }
        } else if (mAlertType == AlertType.PromptAlert) {
            mBinding.btnNo.setOnClickListener { v: View  ->
                dialog.dismiss()

                if (mOnCloseListener != null)
                    mOnCloseListener!!.onAlertAction(false, null)
            }

            mBinding.btnYes.setOnClickListener { v: View ->
                dialog.dismiss()

                if (mOnCloseListener != null)
                    mOnCloseListener!!.onAlertAction(true, null)
            }
        } else {

            mBinding.btnClose.setOnClickListener { v: View ->
                dialog.dismiss()

                if (mOnCloseListener != null)
                    mOnCloseListener!!.onAlertAction(false, null)
            }
        }

        val builder = AlertDialog.Builder(context)

        builder.setCancelable(false)
        builder.setView(mBinding.root)

        dialog = builder.create()
    }

    fun setTitle(title: String): ChiAlerts
    {
        mBinding.lblTitle.text = title
        return this
    }

    fun setMessage(message: String): ChiAlerts
    {
        mBinding.lblMessage.text = message
        return this
    }

    fun setNeutralTitle(title: String): ChiAlerts
    {
        mBinding.btnClose.text = title
        return this
    }

    fun setPositiveTitle(title: String): ChiAlerts {
        mBinding.btnYes.text = title
        return this
    }

    fun setNegativeTitle(title: String): ChiAlerts
    {
        mBinding.btnNo.text = title
        return this
    }

    fun show(): ChiAlerts
    {
        dialog.show()

        return this
    }

    fun setListener(listener: AlertActionListener): ChiAlerts
    {
        mOnCloseListener = listener
        return this
    }

    enum class AlertType
    {
        SuccessAlert,
        InfoAlert,
        WarningAlert,
        ErrorAlert,
        ConfirmAlert,
        PromptAlert
    }

    interface AlertActionListener
    {
        fun onAlertAction(confirmed: Boolean, prompt: String?)
    }

    companion object
    {
        fun make(context: Context, alertType: AlertType): ChiAlerts {
            return ChiAlerts(context, alertType)
        }

        fun success(context: Context): ChiAlerts {
            return ChiAlerts(context, AlertType.SuccessAlert)
        }

        fun info(context: Context): ChiAlerts {
            return ChiAlerts(context, AlertType.InfoAlert)
        }

        fun warn(context: Context): ChiAlerts {
            return ChiAlerts(context, AlertType.WarningAlert)
        }

        fun error(context: Context): ChiAlerts {
            return ChiAlerts(context, AlertType.ErrorAlert)
        }

        fun confirm(context: Context): ChiAlerts {
            return ChiAlerts(context, AlertType.ConfirmAlert)
        }

        fun prompt(context: Context): ChiAlerts {
            return ChiAlerts(context, AlertType.PromptAlert)
        }
    }
}
