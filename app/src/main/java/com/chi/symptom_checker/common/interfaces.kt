package com.chi.symptom_checker.common

import androidx.databinding.ObservableMap
import com.chi.symptom_checker.models.Question

interface SymptomFormListener
{
    fun onFormUpdate(model: ObservableMap<String, Any>?)
    fun onFormBack()
}
