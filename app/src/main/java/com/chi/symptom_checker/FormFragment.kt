package com.chi.symptom_checker

import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableMap
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.chi.symptom_checker.common.SymptomFormListener
import com.chi.symptom_checker.databinding.FormFragmentBinding
import com.chi.symptom_checker.models.Question
import com.codegini.lib.utils.BindingHelpers
import java.util.*

class FormFragment : Fragment()
{
    private lateinit var binding: FormFragmentBinding
    private lateinit var mListener: SymptomFormListener
    lateinit var mContext: Context
    var mFormModel: ObservableMap<String, Any>? = null

    private var layoutManager: LinearLayoutManager? = null

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        mContext = context

        if(context is SymptomFormListener)
        {
            mListener = context
        }
        else
        {
            throw Exception("Should implement HomeActionListener")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.form_fragment, container, false)
        binding.handler = this

        if (mFormModel == null)
            mFormModel = ObservableArrayMap()
        Log.e("FormFragment", "onViewCreated")

        return binding.root
    }

    override fun onResume() {
        super.onResume()
    }

    fun onFormNext(view: View)
    {
        mListener.onFormUpdate(mFormModel)
    }

    fun onFormBack(view: View)
    {
        mListener.onFormBack()
    }

    fun isBackButtonVisible(isVisible: Boolean)
    {
        if (isVisible)
            binding.btnBack.visibility = View.VISIBLE
        else
            binding.btnBack.visibility = View.GONE
    }

    fun setupSingleField(question: Question, questionNo: Int)
    {
        binding.lblQuestionNo.text = questionNo.toString()
        binding.lblQuestionTitle.text = question.text

        val items = arrayOf(
            BindingHelpers.getString(mContext, "yes", com.codegini.lib.R.string.yes),
            BindingHelpers.getString(mContext, "no", com.codegini.lib.R.string.no),
            BindingHelpers.getString(mContext,"dont_know", com.codegini.lib.R.string.dont_know)
        )

        val radioGroup = RadioGroup(mContext)
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        radioGroup.layoutParams = layoutParams
        radioGroup.gravity = Gravity.CENTER_VERTICAL

        for (i in items.indices) {
            val rdbtn = RadioButton(mContext)
            rdbtn.id = i
            rdbtn.text = items[i]
            rdbtn.buttonTintList =
                ColorStateList.valueOf(mContext.getColor(com.codegini.lib.R.color.colorPrimary))
            rdbtn.textSize = 16f
            val params = RadioGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(0, 15, 0, 15)
            rdbtn.layoutParams = params
            radioGroup.addView(rdbtn)
        }

//        binding.radiogroup.setTag(com.codegini.lib.R.id.form_field, field)
//        binding.radiogroup.setTag(com.codegini.lib.R.id.form_model, model)
        mFormModel!!["skip8"] = true
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            val i = radioGroup.checkedRadioButtonId
            mFormModel!!.put("8",items[i])
            mFormModel!!.put("id8",question.items!![0].id)
            mFormModel!!["skip8"] = false
            mFormModel!!["type8"] = question.type
//            if (field.list_form_ids.size > 0) {
//                model["id" + field.name] = field.list_form_ids[0]
//            }
//            model[field.name] = items[i]
//            model["skip" + field.name] = false
        }

        binding.lorTheForm.addView(radioGroup)
    }

    fun setupSingleChoiceField(question: Question, questionNo: Int)
    {
        binding.lblQuestionNo.text = questionNo.toString()
        binding.lblQuestionTitle.text = question.text

        val radioGroup = RadioGroup(mContext)
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        radioGroup.layoutParams = layoutParams
        radioGroup.gravity = Gravity.CENTER_VERTICAL

        for (i in 0 until  question.items!!.size ) {
            val rdbtn = RadioButton(mContext)
            rdbtn.id = i
            rdbtn.text = question.items!![i].name
            rdbtn.buttonTintList =
                ColorStateList.valueOf(mContext.getColor(com.codegini.lib.R.color.colorPrimary))
            rdbtn.textSize = 16f
            val params = RadioGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(0, 15, 0, 15)
            rdbtn.layoutParams = params
            radioGroup.addView(rdbtn)
        }
//        binding.radiogroup.setTag(com.codegini.lib.R.id.form_field, field)
//        binding.radiogroup.setTag(com.codegini.lib.R.id.form_model, model)
        mFormModel!!["skip8"] = true
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            val i = radioGroup.checkedRadioButtonId

            mFormModel!!["8"] = question.items!![i].name
            mFormModel!!["id8"] = question.items!![i].id
            mFormModel!!["id_absent8"] = null
            mFormModel!!["skip8"] = false
            mFormModel!!["type8"] = question.type
        }

        binding.lorTheForm.addView(radioGroup)
    }

    fun setupMultiChoiceField(question: Question, questionNo: Int)
    {
        binding.lblQuestionNo.text = questionNo.toString()
        binding.lblQuestionTitle.text = question.text

        val linearLayout = LinearLayout(mContext)
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        linearLayout.layoutParams = layoutParams
        linearLayout.gravity = Gravity.CENTER_VERTICAL
        linearLayout.orientation = LinearLayout.VERTICAL

        val checkBoxes: MutableList<CheckBox?> = ArrayList()

        for (i in 0 until  question.items!!.size ) {
            val checkBox = CheckBox(mContext)
            checkBox.id = i
            checkBox.text = question.items!![i].name
            checkBox.buttonTintList = ColorStateList.valueOf(mContext.getColor(com.codegini.lib.R.color.colorPrimary))
            checkBox.textSize = 16f
            val params = RadioGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(0, 15, 0, 15)
            checkBox.layoutParams = params
            checkBoxes.add(checkBox)
//            checkBox.setTag(com.codegini.lib.R.id.form_field, field)
//            checkBox.setTag(com.codegini.lib.R.id.form_model, model)
            linearLayout.addView(checkBox)
        }
//        binding.linearlayout.setTag(com.codegini.lib.R.id.form_field, field)
//        binding.linearlayout.setTag(com.codegini.lib.R.id.form_model, model)
//        model["skip" + field.name] = true
        val selectedIdxs = BooleanArray(question.items!!.size)
        val clickListener =
            View.OnClickListener { view: View ->
                if (view !is CheckBox) {
                    return@OnClickListener
                }

//                val f = view.getTag(com.codegini.lib.R.id.form_field) as FormField
//                val m = view.getTag(com.codegini.lib.R.id.form_model) as ObservableMap<String, Any>
                var strSelected: String? = null
                if (mFormModel!!["8"] != null) {
                    strSelected = mFormModel!!["8"].toString()
                }
                var items: Array<String?> = arrayOfNulls<String>(question.items!!.size)
                var ids: Array<String?> = arrayOfNulls<String>(question.items!!.size)
                for (i in 0 until question.items!!.size) {
                    ids[i] = question.items!![i].id
                    items[i] = question.items!![i].name.toString()
                }
                if (strSelected != null) {
                    val stringArray =
                        strSelected.split(",").toTypedArray()
                    var i = 0
                    while (i < items.size) {
                        selectedIdxs[i] = Arrays.asList(*stringArray).contains(
                            items[i]
                        )
                        i++
                    }
                }
                val checkBox = view
                if (checkBox.isChecked) {
                    selectedIdxs[checkBox.id] = checkBox.isChecked
                } else {
                    selectedIdxs[checkBox.id] = checkBox.isChecked
                }
                var str = ""
                var strId = ""
                var strId_absent = ""
                val sb = StringBuilder()
                val sbIds = StringBuilder()
                val sbIds_absent = StringBuilder()
                var idx = 0
                while (idx < items.size) {
                    if (selectedIdxs[idx]) {
                        sb.append(items[idx]).append(",")
                        sbIds.append(ids[idx]).append(",")
                    } else {
                        sbIds_absent.append(ids[idx]).append(",")
                    }
                    idx++
                }
                if (sb.length - 1 != -1) {
                    str = sb.deleteCharAt(sb.length - 1).toString()
                }
                if (sbIds.length - 1 != -1) {
                    strId = sbIds.deleteCharAt(sbIds.length - 1).toString()
                }
                if (sbIds_absent.length - 1 != -1) {
                    strId_absent = sbIds_absent.deleteCharAt(sbIds_absent.length - 1).toString()
                }

                for (selectedIdx in selectedIdxs) {
                    if (selectedIdx) {
                        mFormModel!!["skip8"] = false
                    }
                }

                mFormModel!!["8"] = str
                mFormModel!!["id8"] = strId
                mFormModel!!["id_absent8"] = strId_absent
                mFormModel!!["type8"] = question.type
            }

        linearLayout.setOnClickListener(clickListener)
        for (i in checkBoxes.indices) {
            checkBoxes[i]!!.setOnClickListener(clickListener)
        }

        binding.lorTheForm.addView(linearLayout)
    }

}