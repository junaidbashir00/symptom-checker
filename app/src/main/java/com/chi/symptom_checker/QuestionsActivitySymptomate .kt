package com.chi.symptom_checker

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableField
import androidx.databinding.ObservableMap
import com.chi.symptom_checker.common.GenericAdapter
import com.chi.symptom_checker.models.*
import com.chi.symptom_checker.alert.ChiAlerts
import com.chi.symptom_checker.common.SymptomFormListener
import com.chi.symptom_checker.utils.Preferences
import com.chi.symptom_checker.databinding.QuestionsActivityBinding
import com.codegini.lib.controls.forms.DataFormFragment
import com.codegini.lib.controls.forms.DataFormListener
import com.codegini.lib.controls.forms.DataFormProvider
import com.codegini.lib.controls.forms.DataFormProviderResult
import com.codegini.lib.controls.forms.models.DataForm
import com.codegini.lib.controls.forms.models.DataFormSection
import com.codegini.lib.controls.forms.models.FieldType
import com.codegini.lib.controls.forms.models.FormField
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.nio.channels.NetworkChannel
import java.util.*
import kotlin.collections.ArrayList

class QuestionsActivitySymptomate: AppBaseActivity(), SymptomFormListener
{
    private val binding: QuestionsActivityBinding by ActivityBindingProperty(R.layout.questions_activity)
    private var iReadingId: Int? = 0
    private var mFormModel: ObservableMap<String, Any>? = null
    private var dataFormFragment: DataFormFragment? = null
    private var mDataForm: DataForm? = null
    private var lstData: List<EventQuestion>? = null
    private var mRiskFactors: ArrayList<RiskFactor>? = null
    private var mCommonRisks: ArrayList<String>? = ArrayList()
    private var mGeoRisks: ArrayList<String>? = ArrayList()
    private var mMentionsList: ArrayList<Mentions>? = ArrayList()
    var questionsAdapter: GenericAdapter<Mentions>? = null
    private var sServiceName: String? = ""
    var mDiseases: String? = ""
    var mStateIndex = 0
    var mState = arrayOf<String>("Parse","CommonFactor","Suggest","GeoFactor")
//    var mState: String? = "Parse"
    var mPreviousState: String? = "Parse"
    var mComplain: String? = ""
    var mQuestionNo: Int = 1
    private var lastConvertedRequest: LastConvertedRequest? = null
    private var serverRequestObject: LastConvertedRequest? = null
    private var questionText: String? = null
    private var mQuestionsList: QuestionsList? = QuestionsList(ArrayList())

    private var mFormFragment: FormFragment? = null
    private var mQuestion: Question? = null

    private var isBackTrack = false
    private var mLastAnswerEvidences: ArrayList<SymptomateAnswer>? = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        binding.appBar.lblTitle.text = getString(R.string.symptomate)

        binding.locProblem.visibility = View.VISIBLE
        binding.appBar.tbMain.visibility = View.GONE


        mFormModel = ObservableArrayMap()

        lstData = ArrayList()
        mRiskFactors = ArrayList()
        initLists()
        lastConvertedRequest = LastConvertedRequest(
            null,
            null,
            "male",
            "26",
            ArrayList()
        )
        serverRequestObject = LastConvertedRequest(
            null,
            null,
            "male",
            "26",
            ArrayList()
        )
    }

    override fun onAppbarBack(v: View)
    {
        onConfirmation()
    }

    override fun onBackPressed()
    {
        onConfirmation()
    }

    fun onBtnStart(view: View)
    {
        val problem = binding.txtProblem.editText!!.text.toString()

        if (problem.isEmpty())
        {
            binding.txtProblem.error = "Please describe your problem"
            return
        }

        mComplain = problem
        startDiagnosis(problem)
    }

    fun onBtnChange(view: View)
    {
        binding.locConfirm.visibility = View.GONE
        binding.locProblem.visibility = View.VISIBLE
        binding.appBar.tbMain.visibility = View.GONE
    }

    fun onBtnConfirm(view: View)
    {
        showProgress("Confirming...")
        binding.locConfirm.visibility = View.GONE
        for (mention in mMentionsList!!)
        {
            addEvidence(mention.id!!, mention.choice_id!!, true)
            addEvidenceServer(mention.id!!, mention.choice_id, true, null, null, null, null)
        }

        getRiskFactors()
    }

    fun onBtnDone(view: View)
    {
        finish()
    }

    private fun onConfirmation()
    {
        hideKeyboard()
        val alert = ChiAlerts.confirm(this).setTitle(getString(R.string.data_lost)).setMessage(getString(R.string.going_back_may_lost)).setNegativeTitle(getString(R.string.no)).setPositiveTitle(getString(R.string.yes))
        alert.setListener(object : ChiAlerts.AlertActionListener {
            override fun onAlertAction(confirmed: Boolean, prompt: String?) {
                if (confirmed)
                {
                    this@QuestionsActivitySymptomate.onBack()
                }
            }
        }).show()
    }

    fun onBack()
    {
        super.onBackPressed()
    }

    fun initLists()
    {
        // Common Risk factors list
        mCommonRisks!!.add("p_7")
        mCommonRisks!!.add("p_28")
        mCommonRisks!!.add("p_10")
        mCommonRisks!!.add("p_9")
        mCommonRisks!!.add("p_147")
        mCommonRisks!!.add("p_8")
//        if (RuntimeConfig.getCurrentPatient(this)!!.gender.toLowerCase(Locale.ROOT).equals("female") && RuntimeConfig.getCurrentPatient(this)!!.age > 39)
//        {
//            mCommonRisks!!.add("p_11")
//        }
        // Geo Risk factors list
        mGeoRisks!!.add("p_15")
        mGeoRisks!!.add("p_20")
        mGeoRisks!!.add("p_21")
        mGeoRisks!!.add("p_16")
        mGeoRisks!!.add("p_17")
        mGeoRisks!!.add("p_18")
        mGeoRisks!!.add("p_14")
        mGeoRisks!!.add("p_19")
        mGeoRisks!!.add("p_22")
        mGeoRisks!!.add("p_13")

    }

    fun onCreateForm(question: SymptomateQuestion)
    {
//        val single = "{\"question\": {\"type\": \"single\",\"text\": \"Does the pain increase when you touch or press on the area around your ear?\",\"items\": [{\"id\": \"s_476\",\"name\": \"Pain increases when touching ear area\",\"choices\": [{\"id\": \"present\",\"label\": \"Yes\"},{\"id\": \"absent\",\"label\": \"No\"},{\"id\":\"unknown\",\"label\": \"Don't know\"}]}],\"extras\": {}}}"
//        val group_single = "{\"question\": {\"type\": \"group_single\",\"text\": \"What is your body temperature?\",\"items\": [{\"id\": \"s_99\",\"name\": \"Between 99.5 and 101 °F (37 and 38 °C)\",\"choices\": [{\"id\": \"present\",\"label\": \"Yes\"},{\"id\": \"absent\",\"label\": \"No\"},{\"id\": \"unknown\",\"label\": \"Don't know\"}]},{\"id\": \"s_100\",\"name\": \"Above 101 °F (38 °C)\",\"choices\": [{\"id\": \"present\",\"label\": \"Yes\"},{\"id\": \"absent\",\"label\": \"No\"},{\"id\": \"unknown\",\"label\": \"Don't know\"}]}],\"extras\": {}}}"
//        val group_multiple = "{\"question\": {\"type\": \"group_multiple\",\"text\": \"How would you describe your headache?\",\"items\": [{\"id\": \"s_25\",\"name\": \"Pulsing or throbbing\",\"choices\": [{\"id\": \"present\",\"label\": \"Yes\"},{\"id\": \"absent\",\"label\": \"No\"},{\"id\": \"unknown\",\"label\": \"Don't know\"}]},{\"id\": \"s_604\",\"name\": \"Feels like \\\"stabbing\\\" or \\\"drilling\\\"\",\"choices\": [{\"id\": \"present\",\"label\": \"Yes\"},{\"id\": \"absent\",\"label\": \"No\"},{\"id\": \"unknown\",\"label\": \"Don't know\"}]},{\"id\": \"s_23\",\"name\": \"Feels like pressure around my head\",\"choices\": [{\"id\": \"present\",\"label\": \"Yes\"},{\"id\": \"absent\",\"label\": \"No\"},{\"id\": \"unknown\",\"label\": \"Don't know\"}]}],\"extras\": {}}}"
//        val obj = Preferences.deserialize(single, SymptomateQuestion::class.java)

        questionText = question.question!!.text
        mQuestion = question.question
        this.mFormFragment = FormFragment()
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.lorContent, this.mFormFragment!!)
        fragmentTransaction.commitAllowingStateLoss()

        Handler().postDelayed({
            when
            {
                question.question!!.type.equals("single") -> {
                    mFormFragment!!.setupSingleField(question.question!!, mQuestionNo)
                }
                question.question!!.type.equals("group_single") -> {
                    mFormFragment!!.setupSingleChoiceField(question.question!!, mQuestionNo)
                }
                question.question!!.type.equals("group_multiple") -> {
                    mFormFragment!!.setupMultiChoiceField(question.question!!, mQuestionNo)
                }
            }

            if(mQuestionNo > 1 && !isBackTrack)
                mFormFragment!!.isBackButtonVisible(true)

        },500)

//        val questions = convert(question)
//        lstData = listOf(questions)

//        onGenerateForm()
    }

    private fun startDiagnosis(problem: String)
    {
        showProgress("Starting...")
        val request = "{\"text\": \"$problem\"}"
        val jsonObject = JSONObject(request)
        val body: RequestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (jsonObject).toString())
        val call = apiService!!.GetParse(body)
        call.enqueue(object: Callback<SymptomateMentions> {
            override fun onFailure(call: Call<SymptomateMentions>, t: Throwable) {
                Log.e("QuestionActivity","onFailure Something went wrong.")
                hideProgress()
                showAlert("Connectivity issue","problem getting response from server")
            }
            override fun onResponse(call: Call<SymptomateMentions>, response: Response<SymptomateMentions>) {
                if (response.isSuccessful)
                {
                    hideProgress()
                    hideKeyboard()
                    val question = response.body()
                    if (question!!.mentions!!.size == 0)
                    {
                        binding.txtProblem.error = "Please describe your problem clearly"
                        return
                    }
                    mMentionsList = question.mentions
                    confirmSuggestions()
                }
                else
                {
                    hideProgress()
                    showAlert("Connectivity issue","problem getting response from server")
                    Log.e("QuestionActivity","Something went wrong. ==> ${response.errorBody()?.string()}")
                }
            }
        })
    }

    private fun confirmSuggestions()
    {
        binding.locProblem.visibility = View.GONE
        binding.appBar.tbMain.visibility = View.VISIBLE
        binding.locConfirm.visibility = View.VISIBLE

        questionsAdapter = GenericAdapter(mMentionsList!!, this@QuestionsActivitySymptomate, R.layout.cell_suggestion_list)
        { view: View, it: Mentions ->
            var action = view!!.getTag() as String
            if(action.equals("Delete"))
            {
                deleteSymptom(it)
            }
        }
        binding.rvSuggestion.adapter = questionsAdapter
    }

    private fun deleteSymptom(item: Mentions)
    {
        if (mMentionsList!!.size > 1)
        {
            mMentionsList!!.remove(item)
            questionsAdapter!!.notifyDataSetChanged()
        }
        else
        {
            showAlert("Alert", "You must have at least one symptom")
        }
    }

    private fun getRiskFactors()
    {
        val call = apiService!!.GetRiskFactors()
        call.enqueue(object: Callback<ArrayList<RiskFactor>> {
            override fun onFailure(call: Call<ArrayList<RiskFactor>>, t: Throwable) {
                Log.e("QuestionActivity","onFailure Something went wrong.")
                hideProgress()
                showAlert("Connectivity issue","problem getting response from server")
            }
            override fun onResponse(call: Call<ArrayList<RiskFactor>>, response: Response<ArrayList<RiskFactor>>) {
                if (response.isSuccessful)
                {
                    hideProgress()
                    binding.locProblem.visibility = View.GONE
                    binding.appBar.tbMain.visibility = View.VISIBLE
                    mRiskFactors = response.body()
                    showRiskFactors(true)
                }
                else
                {
                    hideProgress()
                    showAlert("Connectivity issue","problem getting response from server")
                    Log.e("QuestionActivity","Something went wrong. ==> ${response.errorBody()?.string()}")
                }
            }
        })
    }

    fun showRiskFactors(isCommonRisk: Boolean)
    {

        if(isCommonRisk)
        {
//            mPreviousState = mState
//            mState = "CommonFactor"
            mStateIndex++
            val riskFactorQuestion: SymptomateQuestion? = SymptomateQuestion(
                Question(
                    "group_multiple", "Please check all that apply to you.",
                    ArrayList()
                )
            )
            for (i in 0 until mRiskFactors!!.size step 1)
            {
                val riskFactor = mRiskFactors!!.get(i)
                for (risk in mCommonRisks!!)
                {
                    if (risk.equals(riskFactor.id))
                    {
                        riskFactorQuestion!!.question!!.items!!.add(Items(risk,riskFactor.name,null))
                    }
                }
            }
            onCreateForm(riskFactorQuestion!!)
        }
        else
        {
//            mPreviousState = mState
//            mState = "GeoFactor"
            mStateIndex++
            val riskFactorQuestion: SymptomateQuestion? = SymptomateQuestion(
                Question(
                    "group_multiple", "Please select where you live or have recently traveled to.",
                    ArrayList()
                )
            )
            for (i in 0 until mRiskFactors!!.size step 1)
            {
                val riskFactor = mRiskFactors!!.get(i)
                for (risk in mGeoRisks!!)
                {
                    if (risk.equals(riskFactor.id))
                    {
                        val nameText = riskFactor.name!!.replace("Recent travel to ","")
                        riskFactorQuestion!!.question!!.items!!.add(Items(risk,nameText,null))
                    }
                }
            }
            onCreateForm(riskFactorQuestion!!)
        }
    }

    private fun getSuggestions()
    {
//        mPreviousState = mState
//        mState = "Suggest"
        mStateIndex++
        val request = Gson().toJson(getSuggestionRequest())
        val jsonObject = JSONObject(request)
        val body: RequestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (jsonObject).toString())
        val call = apiService!!.GetSuggestion(body)
        call.enqueue(object: Callback<ArrayList<SuggestionResponse>> {
            override fun onFailure(call: Call<ArrayList<SuggestionResponse>>, t: Throwable) {
                Log.e("QuestionActivity","onFailure Something went wrong.")
                hideProgress()
                showAlert("Connectivity issue","problem getting response from server")
            }
            override fun onResponse(call: Call<ArrayList<SuggestionResponse>>, response: Response<ArrayList<SuggestionResponse>>) {
                if (response.isSuccessful)
                {
                    binding.locProblem.visibility = View.GONE
                    binding.appBar.tbMain.visibility = View.VISIBLE
                    val suggestions = response.body()
                    if (suggestions!!.size == 0)
                    {
                        hideProgress()
                        NextQuestion()
                    }
                    else
                    {
                        showSuggestions(suggestions!!)
                        hideProgress()
                    }

                }
                else
                {
                    hideProgress()
                    showAlert("Connectivity issue","problem getting response from server")
                    Log.e("QuestionActivity","Something went wrong. ==> ${response.errorBody()?.string()}")
                }
            }
        })
    }

    fun getSuggestionRequest(): Suggestion
    {
        val request = Suggestion(lastConvertedRequest!!.sex, lastConvertedRequest!!.age, ArrayList())
        for (evidence in lastConvertedRequest!!.evidence!!)
        {
            request.selected!!.add(evidence.id!!)
        }
        return request
    }

    fun showSuggestions(suggestions: ArrayList<SuggestionResponse>)
    {
        val riskFactorQuestion: SymptomateQuestion? = SymptomateQuestion(
            Question(
                "group_multiple", "Do you have any of the following symptoms?",
                ArrayList()
            )
        )
        for (suggestion in suggestions)
        {
            riskFactorQuestion!!.question!!.items!!.add(Items(suggestion.id,suggestion.name,null))
        }

        onCreateForm(riskFactorQuestion!!)
    }

    fun addEvidence(id: String, choice_id : String, initial: Boolean? = null, related: Boolean? = null)
    {
        lastConvertedRequest!!.evidence!!.add(
            SymptomateAnswer(
                id,
                choice_id,
                initial,
                related
            )
        )
    }

    fun addEvidenceServer(id: String, choice_id : String, initial: Boolean? = null, related: Boolean? = null, question: String?, answer: String?, type: String?)
    {
        serverRequestObject!!.evidence!!.add(
            SymptomateAnswer(
                id,
                choice_id,
                initial,
                related,
                question,
                answer,
                type
            )
        )
    }

    fun populateServerRequestData(id: String, choice_id: String, related: Boolean?, question: String?, answer: String?, type: String)
    {
        if (type == "group_multiple")
        {
            for (que in lstData!!)
            {
                for (j in 0 until que.options!!.size step 1)
                {
                    if (que.options!![j].option_id == id)
                    {
                        addEvidenceServer(id, choice_id, null, related, que.options!![j].option_name_to_display, answer, type)
                    }
                }
            }
        }
        else
        {
            addEvidenceServer(id, choice_id, null, related, question, answer, type)
        }
    }

    private fun getSymptomateQuestions()
    {
        val request = Gson().toJson(lastConvertedRequest)
        val jsonObject = JSONObject(request)
        val body: RequestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (jsonObject).toString())
        val call = apiService!!.GetDiagnosis(body)
        call.enqueue(object: Callback<SymptomateQuestion> {
            override fun onFailure(call: Call<SymptomateQuestion>, t: Throwable) {
                Log.e("QuestionActivity","onFailure Something went wrong.")
                hideProgress()
                showAlert("Connectivity issue","problem getting response from server")
            }
            override fun onResponse(call: Call<SymptomateQuestion>, response: Response<SymptomateQuestion>) {
                if (response.isSuccessful)
                {
                    val question = response.body()
                    if (question!!.should_stop!!)
                    {
                        hideProgress()
                        showResult(question)
                    }
                    else
                    {
                        hideProgress()
                        onCreateForm(question)
                    }
                }
                else
                {
                    Log.e("QuestionActivity","Something went wrong. ==> ${response.errorBody()?.string()}")
                    hideProgress()
                    showAlert("Connectivity issue","problem getting response from server")
                }
            }
        })
    }

    fun showResult(result: SymptomateQuestion)
    {
        serverRequestObject!!.complaint = mComplain
        serverRequestObject!!.conditions = result.conditions
        val intent = Intent(this, SummaryActivity::class.java)
        intent.putExtra("result", Preferences.serialize(result))
        intent.putExtra("serverRequest", Preferences.serialize(serverRequestObject))
        intent.putExtra("questions", Preferences.serialize(mQuestionsList))
        startActivity(intent)
        finish()
    }

    fun onSkipQuestion()
    {
        if (mState[mStateIndex].equals("GeoFactor"))
        {
            if (mQuestion!!.type != null)
            {
                if (mQuestion!!.type == "single")
                {
                    addEvidence(mQuestion!!.items!!.get(0).id!!, "unknown", null, null)
                    addEvidenceServer(mQuestion!!.items!!.get(0).id!!, "unknown", null, null, questionText, mQuestion!!.items!!.get(0).name, mQuestion!!.type.toString())
                    return
                }

                for (i in 0 until mQuestion!!.items!!.size step 1)
                {
                    if (mQuestion!!.type == "group_single" || mQuestion!!.type == "group_multiple")
                    {
                        addEvidence(mQuestion!!.items!!.get(i).id!!, "absent", null, null)
                        addEvidenceServer(mQuestion!!.items!!.get(i).id!!, "absent", null, null, mQuestion!!.items!!.get(i).name, "No", mQuestion!!.type.toString())
                    }
                }
            }
        }

        val answer2 = Gson().toJson(lastConvertedRequest)
        Log.e("QuestionsActivity","Symptomate onSkipQuestion ==> $answer2 ")

        NextQuestion()
    }

    private fun NextQuestion()
    {
    when {
        mState[mStateIndex].equals("Parse") -> {
            showRiskFactors(true)
            hideProgress()
        }
        mState[mStateIndex].equals("CommonFactor") -> {
            getSuggestions()
        }
        mState[mStateIndex].equals("Suggest") -> {
            showRiskFactors(false)
            hideProgress()
        }
        mState[mStateIndex].equals("GeoFactor") -> {
            getSymptomateQuestions()
        }
    }
}

    override fun onFormBack()
    {
        isBackTrack = true
        mFormFragment!!.isBackButtonVisible(false)
        val requestCopy = lastConvertedRequest!!.deepCopy

        for (ans in mLastAnswerEvidences!!)
        {
            for (evidence in requestCopy.evidence!!)
            {
                if (ans.id == evidence.id)
                {
                    lastConvertedRequest!!.evidence!!.remove(evidence)
                    break
                }
            }
        }

        mStateIndex -= 2
        NextQuestion()
    }

    override fun onFormUpdate(model: ObservableMap<String, Any>?)
    {
        if (model!!["skip8"] as Boolean)
        {
            showProgress("Loading...")
            val questionsData = QuestionsData(questionText, "Skipped", "null", model["type8"] as String)
            mQuestionsList!!.list!!.add(questionsData)
            onSkipQuestion()
            return
        }

        showProgress("Loading...")
        Log.e("onFormUpdate"," ======  option id ==== ${model!!.get("id8")}")
        Log.e("onFormUpdate"," ======  selected ==== ${model!!.get("8")}")
        Log.e("onFormUpdate"," ======  unselected ==== ${model!!.get("id_absent8")}")
        Log.e("onFormUpdate"," ======  skipped ==== ${model!!.get("skip8")}")
        Log.e("onFormUpdate"," ======  Type ==== ${model!!.get("type8")}")

        saveAnswerData(model)
        mQuestionNo++
        NextQuestion()
        isBackTrack = false
    }

    fun saveAnswerData(data: ObservableMap<String, Any>?)
    {
        mLastAnswerEvidences!!.clear()
        val option_id = data!!.get("id8") as String?
        val selected = data!!.get("8") as String?
        val unselected = data!!.get("id_absent8") as String?
        val skipped = data!!.get("skip8") as Boolean?
        val type = data!!.get("type8") as String?

        val questionsData = QuestionsData(questionText, selected, type)
        mQuestionsList!!.list!!.add(questionsData)

        var related: Boolean? = null
        if (mState[mStateIndex].equals("Suggest"))
            related = true
        else
            related = null

        //For subsequent requests

        var choice_id = "present"
        if (option_id!!.contains(","))
        {
            val stringArray: Array<String> =
                option_id.split(",").toTypedArray()
            for (id in stringArray) {
                addEvidence(id, choice_id, null, related)
                populateServerRequestData(id, choice_id, related, questionText, selected, type!!)
                mLastAnswerEvidences!!.add(SymptomateAnswer(id, choice_id, null, related))
            }
        }
        else
        {
            if (type == "single" && selected.equals("no", true)) {
                choice_id = "absent"
            } else if (type == "single" && selected.equals("don't know", true)) {
                choice_id = "unknown"
            }

            addEvidence(option_id, choice_id, null, related)
            populateServerRequestData(option_id, choice_id, related, questionText, selected, type!!)
            mLastAnswerEvidences!!.add(SymptomateAnswer(option_id, choice_id, null, related))
        }

        // for unselected ids
        if (unselected == null || unselected == "null")
            return

        if (!mState[mStateIndex].equals("Suggest")) {
            if (unselected.contains(","))
            {
                choice_id = "absent"
                val stringArray: Array<String> =
                    unselected.split(",").toTypedArray()
                for (id in stringArray)
                {
                    addEvidence(id, choice_id)
                    populateServerRequestData(id, choice_id,null, questionText, selected, type!!)
                    mLastAnswerEvidences!!.add(SymptomateAnswer(id, choice_id))
                }
            }
            else
            {
                addEvidence(unselected, choice_id)
                populateServerRequestData(unselected, choice_id, null, questionText, selected, type!!)
                mLastAnswerEvidences!!.add(SymptomateAnswer(unselected, choice_id))
            }
        }
    }
}
