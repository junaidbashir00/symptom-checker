package com.chi.symptom_checker

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.chi.symptom_checker.alert.ChiAlerts
import com.chi.symptom_checker.common.GenericAdapter
import com.chi.symptom_checker.databinding.SummaryActivityBinding
import com.chi.symptom_checker.models.*
import com.chi.symptom_checker.utils.Preferences
import com.chi.symptom_checker.utils.Preferences.deserialize
import com.codegini.lib.controls.datatable.DataTableConfig
import com.codegini.lib.controls.datatable.DataTableFragment
import com.codegini.lib.controls.datatable.DataTableListener
import com.codegini.lib.controls.datatable.DataTableProvider
import com.codegini.lib.controls.forms.models.FieldType
import java.util.*

class SummaryActivity : AppBaseActivity(), DataTableProvider, DataTableListener<Condition>
{
    private val binding: SummaryActivityBinding by ActivityBindingProperty(R.layout.summary_activity)
    var resultList: ArrayList<Condition>? = null
    var serverRequest: LastConvertedRequest? = null
    var mQuestionsList: QuestionsList? = QuestionsList(ArrayList())
    var questionsAdapter: GenericAdapter<QuestionsData>? = null
    var rvQuestions: RecyclerView? = null
    private val rotate = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        binding.appBar.lblTitle.text = getString(R.string.summary)

        resultList = ArrayList()

        val intent = intent
        val data = deserialize(intent.getStringExtra("result")!!, SymptomateQuestion::class.java)
        serverRequest = deserialize(intent.getStringExtra("serverRequest")!!, LastConvertedRequest::class.java)
//        mQuestionsList = deserialize(intent.getStringExtra("questions")!!, QuestionsList::class.java)

        createQuestionList(serverRequest!!.evidence)
        PopulateList(data)
        initDataTable()

//        questionsAdapter = GenericAdapter(mQuestionsList!!.list!!, this, R.layout.cell_question_list)
//        { view: View, it: QuestionsData ->
//
//        }
//        binding.rvQuestions.adapter = questionsAdapter
    }

    private fun initDataTable() {
        val fragment =
            DataTableFragment.newInstance<Condition>("summary")
        val fragmentManager =
            this.supportFragmentManager
        val fragmentTransaction =
            fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.lorContent, fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }

    fun PopulateList(data: SymptomateQuestion) {
        resultList!!.addAll(data.conditions!!)
//        Collections.sort(
//            resultList
//        ) {
//                (_, _, _, probability),
//                (_, _, _, probability)
//            -> probability!!.compareTo(probability)
//        }
    }

    fun createQuestionList(resultList: ArrayList<SymptomateAnswer>?)
    {
        mQuestionsList!!.list!!.clear()
        for (result in resultList!!)
        {
            if (result.question != null)
            {
                if (result.question_type == FieldType.DF_SINGLE.toString() || result.question_type == FieldType.DF_MULTI_CHOICE.toString())
                {
                    var choice = "Don't know"
                    when {
                        result.choice_id.equals("present") -> choice = "Yes"
                        result.choice_id.equals("absent") -> choice = "No"
                    }

                    mQuestionsList!!.list!!.add(QuestionsData(result.question, choice, result.id))
                }
                else
                    mQuestionsList!!.list!!.add(QuestionsData(result.question, result.answer, result.id))
            }
        }

        questionsAdapter = GenericAdapter(mQuestionsList!!.list!!, this, R.layout.cell_question_list)
        { view: View, it: QuestionsData ->

        }
        binding.rvQuestions.adapter = questionsAdapter
    }

    override fun onAppbarBack(v: View)
    {
        onConfirmation()
    }

    override fun onBackPressed()
    {
        onConfirmation()
    }

    private fun onConfirmation()
    {
        hideKeyboard()
        val alert = ChiAlerts.confirm(this).setTitle(getString(R.string.data_lost)).setMessage(getString( R.string.going_back_may_lost)).setNegativeTitle(getString(R.string.no)).setPositiveTitle(getString(R.string.yes))
        alert.setListener(object : ChiAlerts.AlertActionListener {
            override fun onAlertAction(confirmed: Boolean, prompt: String?) {
                if (confirmed)
                {
                    finish()
                }
            }
        }).show()
    }

    fun onBtnDone(view: View?)
    {
//        addQuestionInRequest()
//        Handler().postDelayed({
//            sendToServer()
//        }, 1000)
    }

    private fun addQuestionInRequest()
    {
        var mQCount = 0
        for (i in 0 until serverRequest!!.evidence!!.size step 1)
        {
            if (mQuestionsList!!.list!!.get(mQCount).option_id == serverRequest!!.evidence!![i].id)
            {
                serverRequest!!.evidence!![i].question = mQuestionsList!!.list!!.get(mQCount).question
                serverRequest!!.evidence!![i].question_type = mQuestionsList!!.list!!.get(mQCount).question_type
                mQCount++

                if (mQuestionsList!!.list!!.size == mQCount)
                    break
            }
            else if(mQuestionsList!!.list!!.get(mQCount).answer == "Skipped")
            {
//                serverRequest!!.evidence!![i].question = mQuestionsList!!.list!!.get(mQCount).question
//                serverRequest!!.evidence!![i].question_type = mQuestionsList!!.list!!.get(mQCount).question_type
                mQCount++

                if (mQuestionsList!!.list!!.size == mQCount)
                    break
            }
        }
    }

//    private fun sendToServer() {
//        showProgress(getString("submitting_data", R.string.submitting_data), getString("please_wait", R.string.please_wait))
//        val call = apiService.AddSymptomateObservation(serverRequest!!)
//        call.enqueue(object : ApiCallback<ApiSingleResponse<GenericResponse>>(this)
//        {
//            override fun onSuccess(resp: ApiSingleResponse<GenericResponse>)
//            {
//                hideProgress()
//                finish()
//            }
//
//            override fun onError(t: CgThrowable)
//            {
//                hideProgress()
//                showAlert("Error Sending data", t.ErrorMessage)
//            }
//        })
//    }

    override fun getDataTableConfig(p0: DataTableFragment<*>?): DataTableConfig {
        val config = DataTableConfig()
        config.noRecordCell = R.layout.datatable_norecord
        config.cellResourceId = R.layout.summary_table_cell
//        config.animation_type = ItemAnimation.FADE_IN;

        //        config.animation_type = ItemAnimation.FADE_IN;
        config.showSearch = false
        config.showAddButton = false

        return config
    }

    override fun getDataTableListener(p0: DataTableFragment<*>?): DataTableListener<*> {
        return this
    }

    override fun loadData(fragment: DataTableFragment<Condition>?, p1: Int, p2: String?) {
        fragment!!.onAppendData(resultList, resultList!!.size)
    }

    override fun onCellClick(p0: View?)
    {
        val item = p0!!.tag as Condition
        serverRequest!!.target = item.id
        val intent = Intent(this, SymptomExplainActivity::class.java)
        intent.putExtra("explainRequest", Preferences.serialize(serverRequest))
        startActivity(intent)
    }

    override fun onCellLongClick(p0: View?, p1: Condition?): Boolean {
        return false
    }

    override fun onAddClick(p0: DataTableFragment<Condition>?) {}
}
