package com.chi.symptom_checker.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SymptomApiClient()
{
    var builder = OkHttpClient.Builder()
    var client: OkHttpClient? = null
    val apiService: ApiService

    init
    {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        builder = OkHttpClient.Builder()
        client = builder.readTimeout(25, TimeUnit.SECONDS)
            .connectTimeout(25, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                var request: okhttp3.Request = chain.request()

                val builder = request.newBuilder()
                    .addHeader("App-Id", "8676911d")
                    .addHeader("App-Key", "2663d524231ae758f54495d9cfaee842")
                    .addHeader("Dev-Mode", "true")
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Accept", "application/json")

                request = builder.build()
                chain.proceed(request)

            }.build()

        apiService = Retrofit.Builder()
            .baseUrl("https://api.infermedica.com/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client!!)
            .build().create(ApiService::class.java)
    }

    companion object
    {
        private const val TAG = "ApiClient"
        private var _apiClient: SymptomApiClient? = null

        fun apiService(): ApiService
        {
            if (_apiClient == null)
            {
                _apiClient = SymptomApiClient()
            }

            return _apiClient!!.apiService
        }
    }
}