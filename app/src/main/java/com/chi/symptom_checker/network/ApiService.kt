package com.chi.symptom_checker.network

import com.chi.symptom_checker.models.*
import retrofit2.Call
import okhttp3.RequestBody
import retrofit2.http.*


interface ApiService
{
//    @POST("/api/patient_app2/AddSymptomateObservation")
//    fun AddSymptomateObservation(@Body request: LastConvertedRequest): Call<ApiSingleResponse<GenericResponse>>
//
//    @POST("/api/patient_app2/GetSymptomsDetail")
//    fun getSymptomsDetail  (@Body request: SymptomsDetailRequest): Call<ApiSingleResponse<SymptomsDetails>>

    // Symptom checker api calls
    @POST("diagnosis")
    fun GetDiagnosis(@Body request: RequestBody): Call<SymptomateQuestion>

    @POST("parse")
    fun GetParse(@Body request: RequestBody): Call<SymptomateMentions>

    @GET("risk_factors")
    fun GetRiskFactors(): Call<ArrayList<RiskFactor>>

    @POST("suggest")
    fun GetSuggestion(@Body request: RequestBody): Call<ArrayList<SuggestionResponse>>

    @POST("explain")
    fun GetExplaination(@Body request: RequestBody): Call<ExplainResponse>
}