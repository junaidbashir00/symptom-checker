package com.chi.symptom_checker

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.chi.symptom_checker.network.ApiService
import com.chi.symptom_checker.network.SymptomApiClient
import com.chi.symptom_checker.alert.ChiAlerts
import com.codegini.lib.core.BaseActivity
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty


abstract class AppBaseActivity : BaseActivity()
{
    public var mCreated = false

    lateinit var apiService: ApiService

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        apiService = SymptomApiClient.apiService()

        mCreated = true
    }

    override fun onStart()
    {
        super.onStart()
    }

    override fun onStop()
    {
        super.onStop()
    }


    fun onHideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }

    override fun showAlert(title: String, message: String?)
    {
        showChiAlert(title, message, null)
    }

    fun showChiAlert(title: String, message: String?, listener: OnAlertActionListener?)
    {
        if (this is Activity)
        {
            var activity: Activity = this
            if (activity.isFinishing)
            {
                return
            }
        }

        ChiAlerts.error(this).setTitle(title).setMessage(message!!).show()
    }

    fun showChiAlertAndFinish(title: String, message: String)
    {
        if (this is Activity)
        {
            var activity: Activity = this
            if (activity.isFinishing)
            {
                return
            }
        }

        val alert = ChiAlerts.error(this).setTitle(title).setMessage(message)
        alert.setListener(object : ChiAlerts.AlertActionListener
        {
            override fun onAlertAction(confirmed: Boolean, prompt: String?)
            {
                finish()
            }
        }).show()
    }

    override fun attachBaseContext(base: Context)
    {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(base))
    }

    open fun onAppbarBack(v: View)
    {

    }
}

class ActivityBindingProperty<out T : ViewDataBinding>(@LayoutRes private val resId: Int) :
    ReadOnlyProperty<AppBaseActivity, T>
{

    private var binding: T? = null

    override operator fun getValue(thisRef: AppBaseActivity, property: KProperty<*>): T = binding ?: createBinding(thisRef).also { binding = it }

    private fun createBinding(activity: AppBaseActivity): T = DataBindingUtil.setContentView(activity, resId)
}
