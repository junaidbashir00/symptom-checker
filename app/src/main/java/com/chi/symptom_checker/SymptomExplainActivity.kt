package com.chi.symptom_checker

import android.os.Bundle
import android.util.Log
import android.view.View
import com.chi.symptom_checker.common.GenericAdapter
import com.chi.symptom_checker.models.ExplainResponse
import com.chi.symptom_checker.models.ExplainSymptom
import com.chi.symptom_checker.models.LastConvertedRequest
import com.chi.symptom_checker.models.Mentions
import com.chi.symptom_checker.utils.Preferences
import com.chi.symptom_checker.databinding.SymptomExplainActivityBinding
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SymptomExplainActivity : AppBaseActivity() {

    private val binding: SymptomExplainActivityBinding by ActivityBindingProperty(R.layout.symptom_explain_activity)
    var mSupportingList: ArrayList<Mentions>? = ArrayList()
    var mConflictsList: ArrayList<Mentions>? = ArrayList()
    var suggestionsAdapter: GenericAdapter<Mentions>? = null
    var explainRequest: LastConvertedRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        binding.appBar.lblTitle.text = "Symptom Explain"


        explainRequest = Preferences.deserialize(
            intent.getStringExtra("explainRequest")!!,
            LastConvertedRequest::class.java
        )
        explainRequest!!.complaint = null
        explainRequest!!.conditions = null

        getSymptomExplanation()
    }

    override fun onAppbarBack(v: View)
    {
        finish()
    }

    private fun getSymptomExplanation()
    {
        showProgress("Starting...")
        val request = Gson().toJson(explainRequest)
        val jsonObject = JSONObject(request)
        val body: RequestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (jsonObject).toString())
        val call = apiService!!.GetExplaination(body)
        call.enqueue(object: Callback<ExplainResponse> {
            override fun onFailure(call: Call<ExplainResponse>, t: Throwable) {
                Log.e("QuestionActivity","onFailure Something went wrong.")
                hideProgress()
                showAlert("Connectivity issue","problem getting response from server")
            }
            override fun onResponse(call: Call<ExplainResponse>, response: Response<ExplainResponse>) {
                if (response.isSuccessful)
                {
                    hideProgress()
                    hideKeyboard()
                    val explainResponse = response.body()
                    showSupportingEvidence(explainResponse!!.supporting_evidence)
                    showConflictingEvidence(explainResponse.conflicting_evidence)
                }
                else
                {
                    hideProgress()
                    showAlert("Connectivity issue","problem getting response from server")
                    Log.e("QuestionActivity","Something went wrong. ==> ${response.errorBody()?.string()}")
                }
            }
        })
    }

    fun showSupportingEvidence(resultList: List<ExplainSymptom>)
    {
        mSupportingList!!.clear()
        for (result in resultList)
        {
            mSupportingList!!.add(Mentions(result.id, result.common_name, result.common_name, null, null, null))
        }

        suggestionsAdapter = GenericAdapter(mSupportingList!!, this, R.layout.cell_suggestion_list_no_trash)
        { view: View, it: Mentions ->

        }
        binding.rvEvidenceFor.adapter = suggestionsAdapter

        if (mSupportingList!!.size == 0)
        {
            binding.rvEvidenceFor.visibility = View.GONE
            binding.lblNoEvidenceFor.visibility = View.VISIBLE
        }
    }

    fun showConflictingEvidence(resultList: List<ExplainSymptom>)
    {
        mConflictsList!!.clear()
        for (result in resultList)
        {
            mConflictsList!!.add(Mentions(result.id, result.common_name, result.common_name, null, null, null))
        }

        suggestionsAdapter = GenericAdapter(mConflictsList!!, this, R.layout.cell_suggestion_list_no_trash)
        { view: View, it: Mentions ->

        }
        binding.rvEvidenceAgainst.adapter = suggestionsAdapter

        if (mSupportingList!!.size == 0)
        {
            binding.rvEvidenceAgainst.visibility = View.GONE
            binding.lblNoEvidenceAgainst.visibility = View.VISIBLE
        }
    }
}
